import tensorflow as tf
from tensorflow.python.framework.graph_util import convert_variables_to_constants

sess = tf.Session()
saver = tf.train.import_meta_graph("/home/docker/logs-Tacotron-2/taco_pretrained/tacotron_model.ckpt-206500.meta")
saver.restore(sess, tf.train.latest_checkpoint("/home/docker/logs-Tacotron-2/taco_pretrained/tacotron_model.ckpt-206500.data-00000-of-00001"))
graph = tf.get_default_graph()

output_graph_def = convert_variables_to_constants(sess, sess.graph_def, output_node_names = [n.name for n in tf.get_default_graph().as_graph_def().node])

with tf.gfile.FastGFile('output_graph.pb', mode='wb') as f:
    f.write(output_graph_def.SerializeToString())
