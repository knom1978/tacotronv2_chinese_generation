#import uvicorn
from fastapi import FastAPI
from fastapi.responses import FileResponse
#from fastapi.responses import StreamingResponse
from tacotron.pinyin.parse_text_to_pyin import get_pyin
import tacotron_create
from tacotron_hparams import hparams
import os
import hashlib
import tensorflow as tf
from opencc import OpenCC


description = """
## Features
- Tacotron v2 中文 Text to Speech API
- 繁體中文字串轉換成簡體中文字串 API
- 繁體中文字串轉換成拼音字串 API
- 繁體中文字串產生 Mel 頻譜 API
- Container healthy check API
"""

app = FastAPI(
    title="Tacotron2 Text to Speech",
    description=description,
    version="1.0.0",
    contact={
        "name": "Cecil Liu",
        "email": "cecil_liu@umc.com",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
    )


cwd = os.getcwd()
out_dir = os.path.join(cwd, 'tacotron_inference_output')
os.makedirs(out_dir, exist_ok=True)
ckpt_path = os.path.join(cwd, 'logs-Tacotron-2/taco_pretrained')
checkpoint_path = tf.train.get_checkpoint_state(ckpt_path).model_checkpoint_path
step = checkpoint_path.split('/')[-1].split('-')[-1].strip()
synth = tacotron_create.Synthesizer()
synth.load(checkpoint_path, hparams)
print('succeed in loading checkpoint')


@app.get("/text_to_speech", tags=["Audio"],
         responses={
             200: {
                 "content": {"audio/wav": {}},
                 "description": "Return the JSON item or an audio.",
                 },
             422: {
                 "description": "Validation Error",
                 "content": {"application/json": {"schema": {"$ref": "#/components/schemas/HTTPValidationError"}}}
                 },})
def text_to_speech(text:str):
    '''
    轉換繁體中文字串成wav檔, 返回wav檔.
    '''
    cc = OpenCC('t2s')
    text = cc.convert(text)
    pyin, text = get_pyin(text)
    m = hashlib.md5()
    m.update(text.encode('utf-8'))
    idx = m.hexdigest()
    #synth = tacotron_create.Synthesizer()
    filepath = synth.wav_synthesize(pyin, out_dir, idx, step)
    #return StreamingResponse(filepath, media_type="audio/wav")
    return FileResponse(filepath, media_type="audio/wav")
#    return {"filepath": filepath}

@app.get("/chinese_t2s", tags=["Text"])
def transform_tranditional_chinese_to_simplified_chinese(text:str) -> str:
    '''
    轉換繁體中文字串至簡體中文字串, 返回簡體中文字串.
    '''
    cc = OpenCC('t2s')
    text = cc.convert(text)
    return text

@app.get("/chinese_t2pyin", tags=["Text"])
def transform_tranditional_chinese_to_pyin(text:str) -> str:
    '''
    轉換繁體中文字串至拼音list, 返回拼音list.
    '''
    cc = OpenCC('t2s')
    text = cc.convert(text)
    pyin, text = get_pyin(text)
    return pyin

@app.get("/text_to_speech_mel", tags=["Mel Spectrogram"],
         responses={
             200: {
                 "content": {"image/png": {}},
                 "description": "Return the JSON item or an image.",
                 },
             422: {
                 "description": "Validation Error",
                 "content": {"application/json": {"schema": {"$ref": "#/components/schemas/HTTPValidationError"}}}
                 },})
def text_to_speech_mel(text:str):
    '''
    轉換繁體中文字串成Mel頻譜, 返回Mel頻譜圖.
    '''
    cc = OpenCC('t2s')
    text = cc.convert(text)
    pyin, text = get_pyin(text)
    m = hashlib.md5()
    m.update(text.encode('utf-8'))
    idx = m.hexdigest()
    #synth = tacotron_create.Synthesizer()
    filepath = synth.mel_synthesize(pyin, out_dir, idx, step)
    #return StreamingResponse(filepath, media_type="audio/wav")
    return FileResponse(filepath, media_type="image/png")

@app.get("/ping", tags=["Healthy Check"], summary="Check that the service is operational")
def pong():
    """
    Sanity check - this will let the user know that the service is operational.
    It is also used as part of the HEALTHCHECK. Docker uses curl to check that the API service is still running, by exercising this endpoint.
    """
    return {"ping": "pong!"}

'''
if __name__ == "__main__":
    cwd = os.getcwd()
    out_dir = os.path.join(cwd, 'tacotron_inference_output')
    os.makedirs(out_dir, exist_ok=True)
    ckpt_path = os.path.join(cwd, 'logs-Tacotron-2/taco_pretrained')
    checkpoint_path = tf.train.get_checkpoint_state(ckpt_path).model_checkpoint_path
    step = checkpoint_path.split('/')[-1].split('-')[-1].strip()
    synth = tacotron_create.Synthesizer()
    synth.load(checkpoint_path, hparams)
    print('succeed in loading checkpoint')
    #uvicorn.run(app, host="0.0.0.0", port=8000)
'''