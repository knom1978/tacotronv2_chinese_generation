FROM nvidia/cuda:10.0-cudnn7-runtime-ubuntu18.04

ENV PATH /opt/conda/bin:$PATH
ENV LD_LIBRARY_PATH /usr/local/cuda-10.0/lib64:/usr/local/cuda-10.0/extras/CUPTI/lib64:$LD_LIBRARY_PATH

RUN apt-get update --fix-missing && \
    apt-get install -y wget bzip2 ca-certificates curl git libgtk2.0-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-py37_4.8.2-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc

RUN apt-get update --fix-missing && \
    apt-get install -y libsndfile1

RUN pip install --upgrade pip setuptools && \
    pip install --no-cache-dir tensorflow-gpu==1.14 numba==0.48.0 librosa Unidecode matplotlib inflect flask tensorflow-serving-api==1.14 opencc-python-reimplemented

COPY tacotronv2_tts /tacotronv2_tts
RUN mkdir /tacotronv2_tts/tacotron_inference_output
WORKDIR /tacotronv2_tts
CMD ["python3"]

