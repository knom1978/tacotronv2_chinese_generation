# Chinese tacotronv2 env
This **Image/Dockerfile** aims to create a container for the generation of wave file from Chinese text.

Refer to https://github.com/lturing/tacotronv2_wavernn_chinese
And add the tranditional/simple Chinese translation function.

## How to use?
* You should git clone this repository first.
* Then you can build docker image accordingly.
** API server with GPU
```
docker build -f Dockerfile_api -t tacotronv2/tts_gpu_api:1.0 .
```
** API server without GPU
```
docker build -f Dockerfile_api_ubuntu -t tacotronv2/tts_cpu_api:1.0 .
```
** Pure GPU container generation
```
docker build -f Dockerfile -t tacotronv2/tts:1.0 .
```

* Then you can run Chinese text to speech to synthesize the .wav file.
```
docker run --runtime=nvidia --rm -v "$PWD":/tacotronv2_tts/tacotron_inference_output tacotronv2/tts:1.0 python3 tacotron_synthesize.py --text '現在是下午四點三十七分。'
```
Or you can start a FastAPI server
```
docker run --runtime=nvidia --rm -p 8080:8080 tacotronv2/tts_gpu_api:1.0
docker run --rm -p 8080:8080 tacotronv2/tts_cpu_api:1.0
```

Viola! Then you can get a tts application.
